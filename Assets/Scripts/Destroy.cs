﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Destroy : MonoBehaviour
{
	public float lifeTime = 1.0f;
	private float time = 0.0f;

	void Update()
	{
		time += Time.deltaTime;

		if (time >= lifeTime)
		{
			Destroy(gameObject);
		}
	}
}
