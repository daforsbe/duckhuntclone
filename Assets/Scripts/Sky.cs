﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Sky : MonoBehaviour
{
	public Image skyImage;
	public Color warningColor;

	private Color normalColor;

	void Awake()
	{
		normalColor = skyImage.color;
	}

	public void SetWarningColor()
	{
		Utils.SetGraphicRGB(skyImage, warningColor);
	}

	public void ResetWarningColor()
	{
		Utils.SetGraphicRGB(skyImage, normalColor);
	}
}
