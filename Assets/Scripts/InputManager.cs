﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InputManager : MonoBehaviour
{
	//public Texture2D cursorTexture;
	//public Vector2 cursorCenter;
	public LayerMask displayLayer;
	public GameObject VRCamera;
	public GameObject PCCamera;

	private bool vrEnabled = false;
	private Beam beam = null;
	private SteamVR_TrackedController controller = null;
	private Camera PCCameraComponent = null;
	
    public static InputManager Instance { get; private set; }

    public event ClickedEventHandler TriggerClicked;
    public event ClickedEventHandler TriggerUnclicked;

    void Awake()
    {
		Instance = this;
    }

    void Start()
	{
		//Cursor.SetCursor(cursorTexture, cursorCenter, CursorMode.Auto);
		
		vrEnabled = VRCamera != null;

		if (VRCamera != null)
		{
			beam = VRCamera.GetComponentsInChildren<Beam>(true)[ 0 ];
			controller = VRCamera.GetComponentsInChildren<SteamVR_TrackedController>(true)[0];
            controller.TriggerClicked += Controller_TriggerClicked;
            controller.TriggerUnclicked += Controller_TriggerUnclicked;
        }

		if (PCCamera != null)
		{
			PCCameraComponent = PCCamera.GetComponentsInChildren<Camera>(true)[0];
		}
	}

    private void Controller_TriggerClicked(object sender, ClickedEventArgs e)
    {
        Debug.Log("Controller_TriggerClicked");
        TriggerClicked(sender, e);
    }

    private void Controller_TriggerUnclicked(object sender, ClickedEventArgs e)
    {
        TriggerUnclicked(sender, e);
    }
    
	void Update()
	{
		if (!vrEnabled)
		{
			if (Input.GetKeyDown(KeyCode.Mouse0))
			{
				TriggerClicked(this, new ClickedEventArgs());
			}
		}

		Debug.DrawRay(lastRay.origin, lastRay.direction * 10.0f);
	}

	public static bool GetEscDown()
	{
		return Input.GetButtonDown("Cancel");
	}

	public static bool GetUpKeyDown()
	{
		return Input.GetKey(KeyCode.UpArrow);
	}

	public static bool GetDownKeyDown()
	{
		return Input.GetKey(KeyCode.DownArrow);
	}

	public static bool GetLeftKeyDown()
	{
		return Input.GetKey(KeyCode.LeftArrow);
	}

	public static bool GetRightKeyDown()
	{
		return Input.GetKey(KeyCode.RightArrow);
	}

	private Ray lastRay = new Ray();
	
    public bool GetCursorViewspacePosition( out Vector3 position )
    {
		Ray ray = new Ray();

		if (vrEnabled)
		{
			ray.origin = beam.transform.position;
			ray.direction = beam.transform.forward;			
		}
		else
		{
			ray = PCCameraComponent.ScreenPointToRay( Input.mousePosition );
		}

		lastRay = ray;

		RaycastHit hit;
		if (Physics.Raycast(ray, out hit, 10.0f, displayLayer))
		{
			Vector3 textureCoord = hit.textureCoord;
			position = textureCoord;
			textureCoord.z = -1;
			return true;
		}

		position = Vector3.zero;
		return false;
	}
}
