﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Valve.VR;

public class Trigger : MonoBehaviour
{
	public SteamVR_TrackedController controller;
    public Transform pullSoundsParent;
    public Transform releaseSoundsParent;

	public float rotation = 20.0f;

	private Quaternion startRot;
	private const EVRButtonId triggerButton = EVRButtonId.k_EButton_SteamVR_Trigger;
    private AudioSource[] pullSounds = new AudioSource[0];
    private AudioSource[] releaseSounds = new AudioSource[0];

    void Start()
	{
		startRot = transform.localRotation;
        pullSounds = pullSoundsParent.GetComponentsInChildren<AudioSource>(true);
        releaseSounds = releaseSoundsParent.GetComponentsInChildren<AudioSource>(true);

        InputManager.Instance.TriggerClicked += InputManager_TriggerClicked;
        InputManager.Instance.TriggerUnclicked += InputManager_TriggerUnclicked;
    }
    
    private void InputManager_TriggerClicked(object sender, ClickedEventArgs e)
    {
        Debug.Log("InputManager_TriggerClicked");
        pullSounds[Random.Range(0, pullSounds.Length)].Play();
    }

    private void InputManager_TriggerUnclicked(object sender, ClickedEventArgs e)
    {
        releaseSounds[Random.Range(0, releaseSounds.Length)].Play();
    }

    void Update()
	{
		if (controller != null)
		{
			SteamVR_Controller.Device device = SteamVR_Controller.Input((int)controller.controllerIndex);
			float triggerAxis = device.GetAxis(triggerButton).x;
			transform.localRotation = startRot * Quaternion.AngleAxis(rotation * triggerAxis, Vector3.forward);

            if( device.GetPressDown(triggerButton))
            {
                Debug.Log("GetPressDown");
            }
		}
	}
}
