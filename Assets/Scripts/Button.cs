﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Button : MonoBehaviour
{
	public GameObject highlight;

	public enum GameType
	{
		GameA,
		GameB,
		GameC
	}

	public GameType gameType = GameType.GameA;
	
	public void ShowHighlight()
	{
		highlight.SetActive(true);
	}
}
