﻿using UnityEngine;
using UnityEngine.UI;

public class Utils
{
	public static void SetGraphicAlpha(Graphic graphic, float alpha)
	{
		var color = graphic.color;
		color.a = alpha;
		graphic.color = color;
	}

	public static void SetGraphicRGB(Graphic graphic, Color rgb)
	{
		var color = graphic.color;
		color.r = rgb.r;
		color.g = rgb.g;
		color.b = rgb.b;
		graphic.color = color;
	}
}
