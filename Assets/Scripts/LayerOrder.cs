﻿using UnityEngine;

[RequireComponent(typeof(Canvas))]
public class LayerOrder : MonoBehaviour
{
	public float orderInLayer = 0;

	private Canvas canvas;
	
	void Awake()
	{
		canvas = GetComponent<Canvas>();
	}

	void Update()
	{
		canvas.sortingOrder = (int) orderInLayer;
	}
}
