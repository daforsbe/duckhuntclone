﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public enum DroneType
{
	Black,
	Blue,
	Red
}

[RequireComponent(typeof(RectTransform))]
[RequireComponent(typeof(BoxCollider2D))]
[RequireComponent(typeof(Image))]
public class Drone : MonoBehaviour
{
	public float speed = 5.0f;
	public float fallSpeed = 2.0f;

	public RectTransform boundsMin;
	public RectTransform boundsMax;
	public Collider2D boundsCollider;
	public GameManager gameManager;
	public int droneIndex;
	public DroneType droneType;
	public bool playerControlled;
	public bool erratic;
	public AnimationCurve speedBoost;

	private RectTransform rect;
	private BoxCollider2D boxCollider;
	private Vector2 velocity;
	private Vector2 minScreenPoint;
	private Vector2 maxScreenPoint;
	private bool alive;
	private bool flyAway;
	private bool outsideScreen;
	private float aliveTimer;
	private AudioSource loopingSound;
	private float nextTurnTimer;
	private float nextInputTimer;

	void Awake()
	{
		rect = GetComponent<RectTransform>();
		boxCollider = GetComponent<BoxCollider2D>();
	}

	void Start()
	{
		float directionX = Random.Range(0.0f, 1.0f) > 0.5f ? Random.Range(-1.0f, -0.2f) : Random.Range(0.2f, 1.0f);
		float directionY = Random.Range(0.5f, 1.0f);
		Vector2 direction = new Vector2(directionX, directionY).normalized;
		velocity = direction * speed * 0.1f;
		
		Vector2 halfColliderSize = 0.5f * boxCollider.size;
		Vector2 minPoint = boundsMin.anchoredPosition;
		Vector2 maxPoint = boundsMax.anchoredPosition;
		minScreenPoint = minPoint + halfColliderSize;
		maxScreenPoint = maxPoint - halfColliderSize;
		
		alive = true;
		flyAway = false;
		outsideScreen = false;
		aliveTimer = 0.0f;
		nextTurnTimer = GetTurnTimer();
		nextInputTimer = 0.0f;

		switch (droneType)
		{
			case DroneType.Black: loopingSound = AudioManager.Instance.blackDroneLoop; break;
			case DroneType.Blue: loopingSound = AudioManager.Instance.blueDroneLoop; break;
			case DroneType.Red: loopingSound = AudioManager.Instance.redDroneLoop; break;
		}

		AudioManager.Play(loopingSound, 0.0f, 0.2f);
	}

	private float GetTurnTimer()
	{
		switch (droneType)
		{
			case DroneType.Black: return Random.Range(1.0f, 2.0f);
			case DroneType.Blue: return Random.Range(0.75f, 1.5f);
			case DroneType.Red: return Random.Range(0.5f, 1.0f);
		}
		return 1.0f;
	}

	public bool Hit()
	{
		if (alive)
		{
			alive = false;
			transform.Rotate(0, 0, 45.0f);
			velocity = Vector3.down * fallSpeed * 0.1f;
			AudioManager.Stop(loopingSound);
			AudioManager.Play(AudioManager.Instance.droneFallingLoop);
			StartCoroutine(FallRoutine());
			return true;
		}
		return false;
	}

	public bool FlyAway(bool forceUp)
	{
		if (!flyAway && alive)
		{
			gameManager.SetFlyAwayMessage();
			if (forceUp)
				velocity = Vector3.up * velocity.magnitude;
			flyAway = true;
			return true;
		}
		return false;
	}
	
	void Update()
	{
		if (alive && !flyAway)
		{
			if (erratic)
			{
				nextTurnTimer -= Time.deltaTime;

				if (nextTurnTimer <= 0.0f)
				{
					RandomizeDirection(100.0f);
					nextTurnTimer = GetTurnTimer();
				}
			}

			if (playerControlled)
			{
				nextInputTimer -= Time.deltaTime;

				if (nextInputTimer <= 0.0f && ReadPlayerInput())
				{
					nextInputTimer = 0.5f;
				}
			}
		}
		
		Vector2 posDelta2 = velocity * Time.deltaTime;
		Vector3 posDelta3 = new Vector3(posDelta2.x, posDelta2.y, 0.0f);

		if (alive && !flyAway && erratic)
		{
			posDelta3 *= speedBoost.Evaluate(aliveTimer);
		}

		rect.position += posDelta3;

		if (alive && !flyAway)
		{
			aliveTimer += Time.deltaTime;
			if (aliveTimer > 5.0f)
			{
				FlyAway(false);
			}
		}
		
		if (flyAway || !alive)
		{
			if (!Physics2D.IsTouching(boundsCollider, GetComponent<Collider2D>() ) && !outsideScreen )
			{
				outsideScreen = true;
				if (!alive)
				{
					StartCoroutine(CrashRoutine());
				}
				else if (flyAway)
				{
					StartCoroutine(FlyAwayRoutine());
				}
			}
			return;
		}
		
		if (rect.anchoredPosition.x < minScreenPoint.x)
		{
			if (velocity.x > 0)
			{
				velocity.x *= -1;
				RandomizeDirection();
				nextTurnTimer = GetTurnTimer();
			}
		}
		else if (rect.anchoredPosition.y < minScreenPoint.y)
		{
			if (velocity.y < 0)
			{
				velocity.y *= -1;
				RandomizeDirection();
				nextTurnTimer = GetTurnTimer();
			}
		}
		else if (rect.anchoredPosition.x > maxScreenPoint.x)
		{
			if (velocity.x < 0)
			{
				velocity.x *= -1;
				RandomizeDirection();
				nextTurnTimer = GetTurnTimer();
			}
		}
		else if (rect.anchoredPosition.y > maxScreenPoint.y)
		{
			if (velocity.y > 0)
			{
				velocity.y *= -1;
				RandomizeDirection();
				nextTurnTimer = GetTurnTimer();
			}
		}
	}
	
	private bool ReadPlayerInput()
	{
		float speed = velocity.magnitude;
		if (InputManager.GetUpKeyDown())
		{
			velocity = Vector2.up * speed;
			RandomizeDirection(2.0f);
			return true;
		}
		else if (InputManager.GetDownKeyDown())
		{
			velocity = Vector2.down * speed;
			RandomizeDirection(2.0f);
			return true;
		}
		else if (InputManager.GetLeftKeyDown())
		{
			velocity = Vector2.right * speed;
			RandomizeDirection(2.0f);
			return true;
		}
		else if (InputManager.GetRightKeyDown())
		{
			velocity = Vector2.left * speed;
			RandomizeDirection(2.0f);
			return true;
		}
		return false;
	}

	private void RandomizeDirection(float randomMultiplier = 1.0f)
	{
		float speed = velocity.magnitude;
		Vector2 dir = velocity.normalized;
		float randomAmount = 0.0f;
		switch (droneType)
		{
			case DroneType.Black: randomAmount = 0.2f; break;
			case DroneType.Blue: randomAmount = 0.4f; break;
			case DroneType.Red: randomAmount = 0.6f; break;
		}
		dir += randomMultiplier * randomAmount * new Vector2(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f));
		velocity = dir.normalized * speed;
	}
	
	private IEnumerator FlyAwayRoutine()
	{
		AudioManager.Stop(loopingSound, 0.0f, 0.5f);
		yield return new WaitForSeconds(0.5f);
		gameManager.OnDroneFlyAway(droneIndex);
		Destroy(gameObject);
	}

	private IEnumerator FallRoutine()
	{
		while (true)
		{
			transform.Rotate(0, 0, Time.deltaTime * 500.0f);
			yield return null;
		}
	}

	private IEnumerator CrashRoutine()
	{
		AudioManager.Stop(AudioManager.Instance.droneFallingLoop);
		AudioManager.Play(AudioManager.Instance.droneCrash);
		yield return new WaitForSeconds(0.5f);
		gameManager.OnDroneHit(droneIndex);
		Destroy(gameObject);
	}
}
