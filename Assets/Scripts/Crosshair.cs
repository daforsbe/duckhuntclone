﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Crosshair : MonoBehaviour
{
	public Camera canvasCamera;

	private Image image;

	void Awake()
	{
		image = GetComponent<Image>();
	}

	void LateUpdate()
	{
		Vector3 cursorViewspacePos;
		if (InputManager.Instance.GetCursorViewspacePosition(out cursorViewspacePos))
		{
			image.enabled = true;
			Vector3 pos = canvasCamera.ViewportToWorldPoint(cursorViewspacePos);
			pos.z = 0;
			transform.position = pos;
		}
		else
		{
			image.enabled = false;
		}
	}
}
