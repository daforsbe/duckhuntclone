﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using Random = UnityEngine.Random;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
	[HideInInspector] public static readonly int MAX_BULLETS = 3;
	[HideInInspector] public static readonly int MAX_DRONES = 10;

    public Camera canvasCamera;
    public HUD hud;
	public MainMenu mainMenu;
	public GameObject blackDronePrefab;
	public GameObject blueDronePrefab;
	public GameObject redDronePrefab;
	public Transform droneParent;
	public GameObject hitScorePrefab;
	public Transform hitScoresParent;
	public RectTransform droneBoundsMin;
	public RectTransform droneBoundsMax;
	public BoxCollider2D droneBoundsCollider;
	public Transform[] droneSpawnPoints = new Transform[0];
	public LayerMask droneLayerMask;
	public GameObject hitEffectPrefab;
	public Transform hitEffectParent;
	public Animator dogAnimator;
	public Image catchedDrone;
	public Image catchedDrone2;
	public Sky sky;

	private int simulatenousDrones = 1;
	private bool enablePlayer2 = false;
	private bool erraticDroneMovement = false;
	private bool inputsEnabled = false;
	private int ammoCount = MAX_BULLETS;
	private int roundIndex = 1;
	private int droneIndex = -1;
	private int score = 0;
	private int pickupCount = 0;
	private int droneHitCount = 0;
	private int minHitCount = 0;
	private List<Drone> spawnedDrones = new List<Drone>();

    void Start()
    {
        InputManager.Instance.TriggerClicked += OnTriggerClicked;
    }

    void OnTriggerClicked(object sender, ClickedEventArgs e)
    {
        if (inputsEnabled)
        {
            Shoot();
        }
    }

	void Update()
	{
		if (InputManager.GetEscDown())
		{
			mainMenu.topScore.SetScore(score);
			SceneManager.LoadScene(SceneManager.GetActiveScene().name);
		}
	}

	public void StartGameA()
	{
		simulatenousDrones = 1;
		enablePlayer2 = true;
		StartCoroutine(StartGameRoutine());
	}

	public void StartGameB()
	{
		simulatenousDrones = 2;
		erraticDroneMovement = true;
		StartCoroutine(StartGameRoutine());
	}

	public void StartGameC()
	{
		throw new NotSupportedException();
	}
	
	private IEnumerator StartGameRoutine()
	{
		hud.round.SetValue(roundIndex);
		hud.roundPopup.Display(roundIndex, 5.0f);
		minHitCount = 6;
		hud.hits.SetMinHitCount(minHitCount);

		yield return new WaitForSeconds(1.0f);
		yield return PlayGameIntroRoutine();
		
		inputsEnabled = true;
		droneIndex = -1;
		yield return SpawnDrones();
	}

	private IEnumerator EndTickRoutine()
	{
		ClearWarnings();

		if (pickupCount > 0)
		{
			yield return PlayPickupAnimation();
		}
		else
		{
			yield return PlayLaughAnimation();
		}
		
		if (droneIndex == MAX_DRONES - 1)
		{
			yield return EndRoundRoutine();
		}
		else
		{
			pickupCount = 0;
			ammoCount = MAX_BULLETS;
			hud.shots.ResetBullets();

			yield return SpawnDrones();
		}
	}

	private IEnumerator EndRoundRoutine()
	{
		inputsEnabled = false;

		yield return hud.hits.ShuffleAnimationRoutine();

		if (droneHitCount >= minHitCount)
		{
			if (droneHitCount == MAX_DRONES)
			{
				AudioManager.Play(AudioManager.Instance.perfectScoreMusic);
				int perfectScore = GetPerfectScore(roundIndex);
				score += perfectScore;
				hud.score.SetValue(score);
				hud.perfectPopup.Display(perfectScore, 3.0f);
			}
			else
			{
				AudioManager.Play(AudioManager.Instance.roundEndMusic);
			}

			yield return hud.hits.BlinkDronesAnimationRoutine();
		}
		else
		{
			AudioManager.Play(AudioManager.Instance.gameOverMusic);
			mainMenu.topScore.SetScore(score);
			hud.gameOverPopup.Show();
			yield return new WaitForSeconds(1.0f);
			yield return PlayLaughAnimation(false);
			yield return PlayLaughAnimation(false);
			yield return new WaitForSeconds(2.0f);
			SceneManager.LoadScene(SceneManager.GetActiveScene().name);
		}
		
		roundIndex++;
		droneHitCount = 0;
		pickupCount = 0;
		hud.round.SetValue(roundIndex);
		hud.roundPopup.Display(roundIndex, 3.0f);
		hud.hits.ClearDroneIcons();
		minHitCount = GetMinHitCount(roundIndex);
		hud.hits.SetMinHitCount(minHitCount);
		ammoCount = MAX_BULLETS;
		hud.shots.ResetBullets();
		
		yield return PlayGameIntroRoutine();

		inputsEnabled = true;
		droneIndex = -1;
		yield return SpawnDrones();
	}

	private int GetMinHitCount(int roundIndex)
	{
		if (roundIndex <= 10)
			return 6;
		else if (roundIndex <= 12)
			return 7;
		else if (roundIndex <= 14)
			return 8;
		else if (roundIndex <= 19)
			return 9;
		else
			return 10;
	}

	private int GetPerfectScore(int roundIndex)
	{
		if (roundIndex <= 10)
			return 10000;
		else if (roundIndex <= 15)
			return 15000;
		else if (roundIndex <= 20)
			return 20000;
		else
			return 30000;
	}

	private int GetScore(int roundIndex, DroneType type)
	{
		if (roundIndex <= 5)
		{
			switch (type)
			{
				case DroneType.Black: return 500;
				case DroneType.Blue: return 1000;
				case DroneType.Red: return 1500;
			}
		}
		else if (roundIndex <= 10)
		{
			switch (type)
			{
				case DroneType.Black: return 800;
				case DroneType.Blue: return 1600;
				case DroneType.Red: return 2400;
			}
		}
		else
		{
			switch (type)
			{
				case DroneType.Black: return 1000;
				case DroneType.Blue: return 2000;
				case DroneType.Red: return 3000;
			}
		}
		return 0;
	}

	private IEnumerator PlayGameIntroRoutine()
	{
		AudioManager.Play(AudioManager.Instance.introMusic);
		dogAnimator.SetTrigger("Intro");
		yield return new WaitForSeconds(6.0f);
	}

	private IEnumerator PlayPickupAnimation()
	{
		yield return new WaitForSeconds(0.3f);
		AudioManager.Instance.catchDroneMusic.pitch = Random.Range(0.7f, 1.2f);
		AudioManager.Instance.catchDroneMusic.volume = Random.Range(0.5f, 1.0f);
		AudioManager.Play(AudioManager.Instance.catchDroneMusic);
		catchedDrone.gameObject.SetActive(pickupCount > 0);
		catchedDrone2.gameObject.SetActive(pickupCount > 1);
		dogAnimator.SetTrigger("Pickup");
		yield return new WaitForSeconds(1.5f);
		catchedDrone.gameObject.SetActive(false);
		catchedDrone2.gameObject.SetActive(false);
	}

	private IEnumerator PlayLaughAnimation( bool playSound = true)
	{
		yield return new WaitForSeconds(0.3f);
		if(playSound)
			AudioManager.Play(AudioManager.Instance.laughing);
		dogAnimator.SetTrigger("Laugh");
		yield return new WaitForSeconds(2.5f);
	}

	private IEnumerator SpawnDrones()
	{
		for (int i = 0; i < simulatenousDrones; ++i)
		{
			droneIndex++;
			hud.hits.StartHighlight(droneIndex);
			SpawnDrone();
			yield return new WaitForSeconds(0.1f);
		}
	}

	private void SpawnDrone()
	{
		// Make black drones more likely on the round start and 
		// blue and red drones more likely in the round end
		float roundProgress = (float)droneIndex / MAX_DRONES;
		float blackDroneProbability = Mathf.Lerp( 80.0f, 20.0f, roundProgress );
		float blueDroneProbability = Mathf.Lerp( 95.0f, 70.0f, roundProgress );

		int randomInt = Random.Range(0, 100);
		GameObject prefabGO = redDronePrefab;
		if (randomInt <= blackDroneProbability)
			prefabGO = blackDronePrefab;
		else if (randomInt <= blueDroneProbability)
			prefabGO = blueDronePrefab;

		GameObject droneGO = Instantiate(prefabGO);
		droneGO.transform.SetParent(droneParent);
		droneGO.transform.localScale = prefabGO.transform.localScale;

		float spawnPoint = Random.Range(0.0f, 1.0f);
		Transform spawnPointA = droneSpawnPoints[0];
		Transform spawnPointB = droneSpawnPoints[1];

		droneGO.transform.position = Vector3.Lerp(spawnPointA.position, spawnPointB.position, spawnPoint);

		var drone = droneGO.GetComponent<Drone>();
		drone.boundsMin = droneBoundsMin;
		drone.boundsMax = droneBoundsMax;
		drone.gameManager = this;
		drone.droneIndex = droneIndex;
		drone.boundsCollider = droneBoundsCollider;
		drone.playerControlled = enablePlayer2;
		drone.erratic = erraticDroneMovement;

		float speedMultiplier = 1.0f + roundIndex / 10.0f;
		drone.speed *= speedMultiplier;

		spawnedDrones.Add(drone);
	}

	public void OnDroneFlyAway(int index)
	{
		hud.hits.EndHighlight(index);
		
		foreach (var drone in spawnedDrones)
		{
			if (drone.droneIndex == index)
			{
				spawnedDrones.Remove(drone);
				break;
			}
		}

		if (spawnedDrones.Count == 0)
		{
			StartCoroutine(EndTickRoutine());
		}
	}

	public void OnDroneHit(int index)
	{
		foreach (var drone in spawnedDrones)
		{
			if (drone.droneIndex == index)
			{
				spawnedDrones.Remove(drone);
				break;
			}
		}

		if (spawnedDrones.Count == 0)
		{
			StartCoroutine(EndTickRoutine());
		}
	}

	private void Shoot()
	{
		if (ammoCount == 0)
		{
			AudioManager.Play(AudioManager.Instance.emptyClipSound);
			return;
		}

		ammoCount--;
		hud.shots.UseBullet();

		Collider2D hitCollider = null;
		Vector3 mousePos = new Vector3();
        if (InputManager.Instance.GetCursorViewspacePosition(out mousePos))
        {
			var ray = canvasCamera.ViewportPointToRay(mousePos);
			hitCollider = Physics2D.OverlapPoint(ray.origin, droneLayerMask);
		}	

		if (hitCollider != null)
		{
			var drone = hitCollider.transform.GetComponent<Drone>();
			if (drone.Hit())
			{
				pickupCount++;

				droneHitCount++;
				int newScore = GetScore(roundIndex, drone.droneType);
				score += newScore;

				StartCoroutine(ShowScoreRoutine(newScore, drone.transform.position));

				hud.score.SetValue(score);
				hud.hits.EndHighlight(drone.droneIndex);
				hud.hits.SetDroneAsShot(drone.droneIndex);

				Image targetImage = pickupCount == 1 ? catchedDrone : catchedDrone2;
				targetImage.GetComponent<Image>().sprite = drone.GetComponent<Image>().sprite;
				targetImage.GetComponent<Image>().color = drone.GetComponent<Image>().color;

				AudioManager.Play(AudioManager.Instance.hitSound);
			}
			else
			{
				AudioManager.Play(AudioManager.Instance.missSound);
			}
		}
		else
		{
			AudioManager.Play(AudioManager.Instance.missSound);
		}

		if (ammoCount == 0)
		{
			hud.shots.Highlight();

			if (pickupCount < simulatenousDrones)
			{
				bool flyAway = false;
				foreach(var drone in spawnedDrones)
					flyAway |= drone.FlyAway(true);

				if (pickupCount == 0 && flyAway)
					SetFlyAwayMessage();
			}
		}
	}

	private IEnumerator ShowScoreRoutine(int score, Vector3 position)
	{
		yield return new WaitForSeconds(0.1f);

		GameObject hitScore = Instantiate(hitScorePrefab);
		hitScore.transform.SetParent(hitScoresParent);
		hitScore.transform.localScale = hitScorePrefab.transform.localScale;
		hitScore.transform.localRotation = Quaternion.identity;
		hitScore.transform.position = position;
		hitScore.GetComponent<Text>().text = string.Format("{0}", score);
	}

	public void SetFlyAwayMessage()
	{
		SetWarnings();
		hud.flyAwayPopup.Show();
	}

	private void SetWarnings()
	{
		hud.shots.SetWarningColor();
		hud.hits.SetWarningColor();
		sky.SetWarningColor();
	}

	private void ClearWarnings()
	{
		hud.shots.ResetWarningColor();
		hud.hits.ResetWarningColor();
		sky.ResetWarningColor();
		hud.flyAwayPopup.Hide();
		hud.shots.Clear();
	}
}
