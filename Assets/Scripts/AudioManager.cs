﻿using UnityEngine;
using System.Collections;

public class AudioManager : MonoBehaviour
{
	public AudioSource mainThemeLoop;
	public AudioSource introMusic;
	public AudioSource catchDroneMusic;
	public AudioSource laughing;
	public AudioSource roundEndMusic;
	public AudioSource perfectScoreMusic;
	public AudioSource gameOverMusic;
	public AudioSource blackDroneLoop;
	public AudioSource blueDroneLoop;
	public AudioSource redDroneLoop;
	public AudioSource droneFallingLoop;
	public AudioSource droneCrash;
	public AudioSource droneIconMove;
	public AudioSource hitSound;
	public AudioSource missSound;
	public AudioSource emptyClipSound;

	public static AudioManager Instance { get; private set; }

	void Awake()
	{
		Instance = this;
	}

	void Start()
	{
		Play(mainThemeLoop);
	}
	
	public static void Play(AudioSource source, float delay = 0.0f, float fadeIn = 0.0f)
	{
		if (source != null)
		{
			Instance.StartCoroutine(PlayRoutine(source, delay, fadeIn));
		}
	}

	public static void Stop(AudioSource source, float delay = 0.0f, float fadeOut = 0.0f)
	{
		if (source != null)
		{
			Instance.StartCoroutine(StopRoutine(source, delay, fadeOut));
		}
	}

	private static IEnumerator PlayRoutine(AudioSource source, float delay, float fadeIn)
	{
		if (delay > 0.0f)
		{
			yield return new WaitForSeconds(delay);
		}
		
		source.Play();

		if (fadeIn > 0)
		{
			source.volume = 0.0f;
			for (float time = 0.0f; time <= fadeIn; time += Time.deltaTime)
			{
				float t = time / fadeIn;
				source.volume = t;
				yield return null;
			}
			source.volume = 1.0f;
		}
	}

	private static IEnumerator StopRoutine(AudioSource source, float delay, float fadeOut)
	{
		if (delay > 0.0f)
		{
			yield return new WaitForSeconds(delay);
		}

		if (fadeOut > 0)
		{
			source.volume = 1.0f;
			for (float time = 0.0f; time <= fadeOut; time += Time.deltaTime)
			{
				float t = time / fadeOut;
				source.volume = 1.0f - t;
				yield return null;
			}
			source.volume = 0.0f;
		}

		source.Stop();
	}

}
