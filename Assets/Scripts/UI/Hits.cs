﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class Hits : MonoBehaviour
{
	public Transform droneIconsParent;
	public Transform minHitIconsParent;
	public AnimationCurve highlight;
	public Color shotColor;
	public Color warningColor;

	private Image[] droneIcons = new Image[0];
	private Image[] minHitIcons = new Image[0];
	private bool[] shotStatus = new bool[0];
	private Coroutine[] highlightRoutines = new Coroutine[0];
	private Color normalDroneColor;
	private Color normalMinHitColor;

	void Awake()
	{
		droneIcons = droneIconsParent.GetComponentsInChildren<Image>();
		minHitIcons = minHitIconsParent.GetComponentsInChildren<Image>();
		shotStatus = new bool[droneIcons.Length];
		highlightRoutines= new Coroutine[droneIcons.Length];
		for ( int i = 0; i < shotStatus.Length; ++i )
			shotStatus[ i ] = false;
		normalDroneColor = droneIcons[0].color;
		normalMinHitColor = minHitIcons[0].color;
	}

	public void SetMinHitCount(int count)
	{
		for (int i = 0; i < minHitIcons.Length; ++i)
		{
			float alpha = i < count ? 1.0f : 0.0f;
			Utils.SetGraphicAlpha(minHitIcons[i], alpha);
		}
	}

	public void SetWarningColor()
	{
		foreach( var icon in minHitIcons)
			Utils.SetGraphicRGB(icon, warningColor);
	}

	public void ResetWarningColor()
	{
		foreach (var icon in minHitIcons)
			Utils.SetGraphicRGB(icon, normalMinHitColor);
	}

	public void StartHighlight(int index)
	{
		highlightRoutines[index] = StartCoroutine(LoopingHighlightRoutine(droneIcons[index]));
	}

	public void EndHighlight(int index)
	{
		StopCoroutine(highlightRoutines[index]);
		Utils.SetGraphicAlpha(droneIcons[index], 1.0f);
	}

	public void SetDroneAsShot(int index)
	{
		Utils.SetGraphicRGB(droneIcons[index], shotColor);
		shotStatus[index] = true;
	}
	
	public void ClearDroneIcons()
	{
		foreach (var icon in droneIcons)
			Utils.SetGraphicRGB(icon, normalDroneColor);
		for (int i = 0; i < shotStatus.Length; ++i)
			shotStatus[i] = false;
	}

	private IEnumerator LoopingHighlightRoutine(Graphic graphic)
	{
		for (float time = 0; ; time += Time.deltaTime)
		{
			float alpha = highlight.Evaluate(time);
			Utils.SetGraphicAlpha(graphic, alpha);
			yield return null;
		}
	}

	public IEnumerator ShuffleAnimationRoutine()
	{
		int totalShotCount = 0;
		for (int i = 0; i < droneIcons.Length; ++i)
		{
			if (shotStatus[i])
				totalShotCount++;
		}

		for (int i = 0; i < totalShotCount; ++i)
		{
			if (!shotStatus[i])
			{
				Shuffle(i);
				i--;
				AudioManager.Play(AudioManager.Instance.droneIconMove);
				yield return new WaitForSeconds(0.5f);
			}
		}
	}

	public IEnumerator BlinkDronesAnimationRoutine()
	{
		for (int blinkIndex = 0; blinkIndex < 6; ++blinkIndex)
		{
			bool visisble = blinkIndex % 2 == 0;
			for (int i = 0; i < shotStatus.Length; ++i)
			{
				Color color = visisble && shotStatus[i] ? shotColor : normalDroneColor;
				Utils.SetGraphicRGB(droneIcons[i], color);
			}
			yield return new WaitForSeconds(0.5f);
		}
	}

	private void Shuffle(int index)
	{
		int lastIndex = droneIcons.Length - 1;
		for (int i = index; i < lastIndex; ++i)
		{
			shotStatus[i] = shotStatus[i + 1];
			droneIcons[i].color = droneIcons[i + 1].color;
		}
		shotStatus[lastIndex] = false;
		droneIcons[lastIndex].color = normalDroneColor;
	}

}
