﻿using System.Collections;
using UnityEngine;

public class Popup : MonoBehaviour
{
	public void Show()
	{
		gameObject.SetActive(true);
	}

	public void Hide()
	{
		StopAllCoroutines();
		gameObject.SetActive(false);
	}

	public void Display(float duration)
	{
		Show();
		StartCoroutine(HideDelayedRoutine(duration));
	}

	private IEnumerator HideDelayedRoutine(float duration)
	{
		yield return new WaitForSeconds(duration);
		Hide();
	}
}
