﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Shots : MonoBehaviour
{
	public Text shotLabel;
	public Image[] bulletIcons = new Image[GameManager.MAX_BULLETS];
	public Color labelWarningColor = Color.white;
	public AnimationCurve highlight;
	
	private Color labelColor;
	private int visibleBulletIcons = GameManager.MAX_BULLETS;
	
	void Awake()
	{
		labelColor = shotLabel.color;
	}
	
	public void UseBullet()
	{
		Debug.Assert(visibleBulletIcons > 0);
		visibleBulletIcons--;
		Utils.SetGraphicAlpha(bulletIcons[visibleBulletIcons], 0.0f);
	}

	public void ResetBullets()
	{
		foreach (var image in bulletIcons)
			Utils.SetGraphicAlpha(image, 1.0f);
		visibleBulletIcons = GameManager.MAX_BULLETS;
	}

	public void SetWarningColor()
	{
		Utils.SetGraphicRGB(shotLabel, labelWarningColor);
	}

	public void ResetWarningColor()
	{
		Utils.SetGraphicRGB(shotLabel, labelColor);
	}

	public void Highlight()
	{
		StartCoroutine(HighlightRoutine());
	}

	public void Clear()
	{
		StopAllCoroutines();
		Utils.SetGraphicAlpha(shotLabel, 1.0f);
	}

	private IEnumerator HighlightRoutine()
	{
		if (highlight.length == 0)
			yield break;

		float length = highlight.keys[highlight.length - 1].time;
		for (float time = 0; time < length; time += Time.deltaTime)
		{
			float alpha = highlight.Evaluate(time);
			Utils.SetGraphicAlpha(shotLabel, alpha);
			yield return null;
		}
		Utils.SetGraphicAlpha(shotLabel, 1.0f);
	}
}
