﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class ValuePopup : ValueLabel
{
	public void Show()
	{
		gameObject.SetActive(true);
	}

	public void Hide()
	{
		gameObject.SetActive(false);
	}

	public void Display(int value, float duration)
	{
		SetValue(value);
		Show();
		StartCoroutine(HideDelayed(duration));
	}

	private IEnumerator HideDelayed(float duration)
	{
		yield return new WaitForSeconds(duration);
		Hide();
	}

}
