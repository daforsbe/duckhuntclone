﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class TopScore : MonoBehaviour
{
	private Text text;
	private string unformattedScore;
	private int topScore;

	public void SetScore(int score)
	{
		if (score > topScore)
		{
			topScore = score;
			RefreshScoreText();
			SaveScore();
		}
	}

	void Awake()
	{
		text = GetComponent<Text>();
		unformattedScore = text.text;
	}

	void Start()
	{
		LoadScore();
		RefreshScoreText();
	}
	
	private void LoadScore()
	{
		topScore = PlayerPrefs.GetInt("score");
	}

	private void SaveScore()
	{
		PlayerPrefs.SetInt("score", topScore);
		PlayerPrefs.Save();
	}

	private void RefreshScoreText()
	{
		text.text = string.Format(unformattedScore, topScore);
	}
}
