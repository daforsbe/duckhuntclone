﻿using UnityEngine;

public class HUD : MonoBehaviour
{
	public Round round;
	public Shots shots;
	public Hits hits;
	public Score score;
	public ValuePopup roundPopup;
	public Popup flyAwayPopup;
	public ValuePopup perfectPopup;
	public Popup gameOverPopup;
}
