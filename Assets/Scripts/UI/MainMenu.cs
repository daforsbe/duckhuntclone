﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

[RequireComponent(typeof(CanvasGroup))]
public class MainMenu : MonoBehaviour
{
	public GameManager gameManager;
	public AudioManager audioManager;
	public TopScore topScore;
	public LayerMask uiMask;

	private CanvasGroup group;

	private Button[] allButtons = new Button[0];

	void Awake()
	{
		group = GetComponent<CanvasGroup>();
	}

	void Start()
	{
		AudioManager.Play(AudioManager.Instance.mainThemeLoop, 0, 0.2f);

		allButtons = FindObjectsOfType<Button>();

		InputManager.Instance.TriggerClicked += InputManager_TriggerClicked;
	}
	
	void Update()
	{
		foreach (var b in allButtons)
		{
			b.highlight.SetActive(false);
		}

		Vector3 mousePos = new Vector3();
		if (group.blocksRaycasts && InputManager.Instance.GetCursorViewspacePosition(out mousePos))
		{
			var ray = gameManager.canvasCamera.ViewportPointToRay(mousePos);
			Collider2D hitCollider = Physics2D.OverlapPoint(ray.origin, uiMask);
			if (hitCollider)
			{
				var button = hitCollider.GetComponent<Button>();
				button.ShowHighlight();
			}
		}
	}

	private void InputManager_TriggerClicked(object sender, ClickedEventArgs e)
	{
		if (group.blocksRaycasts)
		{
			Vector3 mousePos = new Vector3();
			if (InputManager.Instance.GetCursorViewspacePosition(out mousePos))
			{
				var ray = gameManager.canvasCamera.ViewportPointToRay(mousePos);
				Collider2D hitCollider = Physics2D.OverlapPoint(ray.origin, uiMask);

				if (hitCollider)
				{
                    AudioManager.Play(AudioManager.Instance.hitSound);

                    var button = hitCollider.GetComponent<Button>();
                    switch (button.gameType)
                    {
                        case Button.GameType.GameA:
                            OnGameAPressed();
                            break;
                        case Button.GameType.GameB:
                            OnGameBPressed();
                            break;
                        case Button.GameType.GameC:
                            OnGameCPressed();
                            break;
                    }
                }
                else
                {
                    AudioManager.Play(AudioManager.Instance.missSound);
                }
			}
		}
	}
	
	public void Show()
	{
		group.alpha = 1;
		group.blocksRaycasts = true;
	}

	public void Hide()
	{
		group.alpha = 0;
		group.blocksRaycasts = false;
	}

	public void OnGameAPressed()
	{
		Debug.Log( "Game A Started" );
		OnGameButtonPressed();
		gameManager.StartGameA();
	}

	public void OnGameBPressed()
	{
		Debug.Log( "Game B Started" );
		OnGameButtonPressed();
		gameManager.StartGameB();
	}

	public void OnGameCPressed()
	{
		Debug.Log( "Game C Started" );
		OnGameButtonPressed();
		gameManager.StartGameC();
	}

	public void OnExitPressed()
	{
#if UNITY_EDITOR
		UnityEditor.EditorApplication.isPlaying = false;
#else
		Application.Quit();
#endif
	}

	private void OnGameButtonPressed()
	{
		Hide();
		AudioManager.Stop(AudioManager.Instance.mainThemeLoop, 0, 0.2f);
	}
}
