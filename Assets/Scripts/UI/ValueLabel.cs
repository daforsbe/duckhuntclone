﻿using UnityEngine;
using UnityEngine.UI;

public class ValueLabel : MonoBehaviour
{
	public Text text;
	public string format;
	private int value = 0;
	
	protected void Start()
	{
		RefreshText();
	}

	public void SetValue(int newValue)
	{
		value = newValue;
		RefreshText();
	}

	private void RefreshText()
	{
		text.text = string.Format(format, value);
	}
}
